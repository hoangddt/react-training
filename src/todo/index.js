import React, { Component } from 'react';

class TodoPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            childBackground: "red"
        }
    }
    onChangeText = (event) => {
        // console.log('1..onChange')
        this.setState({
            childBackground: event.target.value,
        });
    }
    componentWillReceiveProps = (props, state) => { // arrow fucnction
        console.log('0c.componentWillReceiveProps')
    }
    shouldComponentUpdate = (props, state) => {
        console.log('1c. shouldComponentUpdate')
        return false;
    }
    onPressButton = () => {
        setTimeout(() => {
            this.setState({
                childBackground: 'black'
            })
        }, 5000)
    }
    render() {
        console.log('2c .Rendering')
        const { childBackground } = this.state; 
        // const childBackground = this.state.childBackground
        const { name, titleColor} = this.props;
        // console.log('>>>>>Tau props ne', this.props)

        return (
            <div style={{ backgroundColor: childBackground, flex: 1}}>
                <h1 style={{ color: titleColor }}> {`Hi ${name}`} </h1>
                <input type="text" name="name" value={childBackground} onChange={this.onChangeText}/>
                <h1>{`titleColor: ${titleColor}`}</h1>
                <button onClick={this.onPressButton} >
                Change color after 5s
                </button>
            </div>
        )
    }
}
export default TodoPage;
