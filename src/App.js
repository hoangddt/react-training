import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import TodoPage from './todo';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      titleColor: 'white'
    }
  }
  onChange = (event) => {
    // this.state.titleColor = event.target.value;
    console.log('1p, onChange')
    this.setState({
      titleColor: event.target.value,
    })
  }
  shouldComponentUpdate(nextProps, nextState) {
    console.log('2p shouldComponentUpdate: nextState', nextState)
    console.log('2p state', this.state)
    return true;
  }
  render() {
    console.log('9p. REnderring')
    console.log('this.state', this.state)
    return (
      <div className="App" style={{ backgroundColor: 'pink'}}>
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h4> Parent-input: </h4>
          <input value={this.state.titleColor} onChange={this.onChange} />
        </header>
        <TodoPage titleColor={this.state.titleColor} name={'duong'} age={'2'} />
      </div>
    );
  }
}

export default App;


// Truyen tu thang con => thang cha
